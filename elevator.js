{
    init: function(elevators, floors) {
        var pressed = [];
        elevators.forEach(function(elv, i, arrElv) {
        var elevator = elv; 

        // Whenever the elevator is idle (has no more queued destinations) ...
        elevator.on("idle", function() {           
            if(pressed.length > 0) {
                var t = pressed.shift();
                console.log('go to ' + t);
                elevator.destinationQueue = [];
                elevator.checkDestinationQueue();
                elevator.goToFloor(t);
            }
        });
        elevator.on("floor_button_pressed", function(floorNum) {
            
            elevator.goToFloor(floorNum);
            pressed = pressed.filter(function(value, index, arr){ 
                return value != floorNum;
            });
        } );
        floors.forEach(function(floor, i, arr) {
            floor.on("up_button_pressed", function() { 
                pressed.push(floor.floorNum());
            } );
            floor.on("down_button_pressed", function() { 
                pressed.push(floor.floorNum());
            } );
        });
      });
    },
    update: function(dt, elevators, floors) {
        // We normally don't need to do anything here
    }
}